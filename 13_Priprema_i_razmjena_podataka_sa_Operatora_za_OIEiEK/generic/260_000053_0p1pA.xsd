<?xml version="1.0" encoding="UTF-8"?>
<!-- ================================================================================ -->
<!-- ==== SubsetMeasurementUnitCommonCode - Code List Schema Module              ==== -->
<!-- ================================================================================ -->
<!--
Schema agency:     ebIX
Schema version:    0.1.A
Schema date:       January 25, 2017

Code list name:    MeasurementUnitCommonCode
Code list agency:  ebix 
Code list version: 0.1.A

Copyright (C) ebIX (2010). All Rights Reserved.

This document is created by using the ebIX Transformation Tool version of November 2016.

This document and translations of it may be copied and furnished to others,
and derivative works that comment on or otherwise explain it or assist in
its implementation may be prepared, copied, published and distributed, in
whole or in part, without restriction of any kind, provided that the above
copyright notice and this paragraph are included on all such copies and
derivative works. However, this document itself may not be modified in any
way, such as by removing the copyright notice or references to ebIX, except
as needed for the purpose of developing ebIX specifications, in which case
the procedures for copyrights defined in the UN/CEFACT Intellectual Property
Rights document must be followed, or as required to translate it into
languages other than English.
The limited permissions granted above are perpetual and will not be revoked
by ebIX or its successors or assigns.
This document and the information contained herein is provided on an "AS IS"
basis and ebIX DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT
INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS
FOR A PARTICULAR PURPOSE.
-->
<xsd:schema
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:bcl260_000053_01A="un:unece:260:data:EEM"
    xmlns:ccts="urn:un:unece:uncefact:documentation:common:3:standard:CoreComponentsTechnicalSpecification:3"
    xmlns:xbt="urn:un:unece:uncefact:data:common:1:draft"
    targetNamespace="un:unece:260:data:EEM"
    elementFormDefault="qualified"
    attributeFormDefault="unqualified"
    version="0.1.A">
    <!-- ================================================================================ -->
    <!-- ==== Imports                                                                ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== Root element for this code list                                        ==== -->
    <!-- ================================================================================ -->
    <xsd:element name="SubsetMeasurementUnitCommonCode" 
        type="bcl260_000053_01A:SubsetMeasurementUnitCommonCodeContentType"/>

    <!-- ================================================================================ -->
    <!-- ==== Content type for this code list                                        ==== -->
    <!-- ================================================================================ -->
    <xsd:simpleType name="SubsetMeasurementUnitCommonCodeContentType">
        <xsd:restriction base="xsd:token">
        <xsd:enumeration value="KWH">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>KWH</ccts:Name>
                 <ccts:Description>Kilowatt hour</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="MAW">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>MAW</ccts:Name>
                 <ccts:Description>Megawatt</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="KWT">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>KWT</ccts:Name>
                 <ccts:Description>Kilowatt</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="K3">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>K3</ccts:Name>
                 <ccts:Description>kVArh (kVA reactive-hour)</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="MTQ">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>MTQ</ccts:Name>
                 <ccts:Description>Cubic Meter</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="MQH">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>MQH</ccts:Name>
                 <ccts:Description>Cubic Meter per Hour</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="D90">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>D90</ccts:Name>
                 <ccts:Description>Cubic meter (net)</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="HUR">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>HUR</ccts:Name>
                 <ccts:Description>Hour</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="MIN">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>MIN</ccts:Name>
                 <ccts:Description>Minute</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="MON">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>MON</ccts:Name>
                 <ccts:Description>Month</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="ANN">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>ANN</ccts:Name>
                 <ccts:Description>Year</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="DAY">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>DAY</ccts:Name>
                 <ccts:Description>Day</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="MWH">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>MWH</ccts:Name>
                 <ccts:Description>Megawatt hour</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="AMP">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>AMP</ccts:Name>
                 <ccts:Description>ampere</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="NM3">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>NM3</ccts:Name>
                 <ccts:Description>Normalised cubic metre</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="B8">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>B8</ccts:Name>
                 <ccts:Description>Joule per cubic metre</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="Q37">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>Q37</ccts:Name>
                 <ccts:Description>Standard cubic metre per day</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="Q38">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>Q38</ccts:Name>
                 <ccts:Description>Standard cubic metre per hour</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="Q39">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>Q39</ccts:Name>
                 <ccts:Description>Normalized cubic metre per day</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="Q40">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>Q40</ccts:Name>
                 <ccts:Description>Normalized cubic metre per hour</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="SM3">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>SM3</ccts:Name>
                 <ccts:Description>Standard cubic metre</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E46">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E46</ccts:Name>
                 <ccts:Description>Kilowatt hour per cubic metre</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="KWN">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>KWN</ccts:Name>
                 <ccts:Description>Kilowatt hour per normalized cubic metre</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="KWS">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>KWS</ccts:Name>
                 <ccts:Description>Kilowatt hour per standard cubic metre</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="Q41">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>Q41</ccts:Name>
                 <ccts:Description>Joule per normalized cubic metre</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="Q42">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>Q42</ccts:Name>
                 <ccts:Description>Joule per standard cubic metre</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="G52">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>G52</ccts:Name>
                 <ccts:Description>Cubic metre per day</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
    <!-- Additional measurement unit value -->
    <xsd:enumeration value="KW">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>KW</ccts:Name>
                 <ccts:Description>Kilowatt</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        </xsd:restriction>
    </xsd:simpleType>
</xsd:schema>  
