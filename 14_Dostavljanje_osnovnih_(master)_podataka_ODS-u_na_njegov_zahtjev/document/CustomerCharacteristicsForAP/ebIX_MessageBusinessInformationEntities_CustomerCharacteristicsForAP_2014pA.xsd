<?xml version="1.0" encoding="UTF-8"?>
<!-- ================================================================================ -->
<!-- ==== Message Business Information Entities XML Schema Module                ==== -->
<!-- ================================================================================ -->
<!--
Schema agency:     ebIX
Schema version:    2014.A
Schema date:       January 25, 2017

Copyright (C) ebIX (2010). All Rights Reserved.

This document is created by using the ebIX Transformation Tool version of November 2016.

This document and translations of it may be copied and furnished to others,
and derivative works that comment on or otherwise explain it or assist in
its implementation may be prepared, copied, published and distributed, in
whole or in part, without restriction of any kind, provided that the above
copyright notice and this paragraph are included on all such copies and
derivative works. However, this document itself may not be modified in any
way, such as by removing the copyright notice or references to ebIX, except
as needed for the purpose of developing ebIX specifications, in which case
the procedures for copyrights defined in the UN/CEFACT Intellectual Property
Rights document must be followed, or as required to translate it into
languages other than English.
The limited permissions granted above are perpetual and will not be revoked
by ebIX or its successors or assigns.
This document and the information contained herein is provided on an "AS IS"
basis and ebIX DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT
INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS
FOR A PARTICULAR PURPOSE.
-->
<xsd:schema
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:mie="un:unece:260:data:EEM-NotifyMPCharacteristics"
    xmlns:ccts="urn:un:unece:uncefact:documentation:common:3:standard:CoreComponentsTechnicalSpecification:3"
    xmlns:xbt="urn:un:unece:uncefact:data:common:1:draft"
    xmlns:bie="un:unece:260:data:EEM"
    xmlns:mdt="un:unece:260:data:EEM-NotifyMPCharacteristics"
    targetNamespace="un:unece:260:data:EEM-NotifyMPCharacteristics"
    elementFormDefault="qualified"
    attributeFormDefault="unqualified"
    version="2014.A">
    <!-- ================================================================================ -->
    <!-- ==== Imports                                                                ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== Inclusions                                                             ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== Inclusion of Message Data Types                                        ==== -->
    <!-- ================================================================================ -->
    <xsd:include schemaLocation="ebIX_MessageDataType_CustomerCharacteristicsForAP_2014pA.xsd"/>
    
    <!-- ================================================================================ -->
    <!-- ==== Message Business Information Entities Definitions                      ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== EnergyParty Type                                                       ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyParty">
        <xsd:sequence>
            <xsd:element name="Identification" type="mdt:PartyIdentifierType_000112" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="SenderEnergyParty" type="mie:EnergyParty"/>
    <xsd:element name="RecipientEnergyParty" type="mie:EnergyParty"/>
    <!-- ================================================================================ -->
    <!-- ==== DomainLocation Type                                                    ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="DomainLocation">
        <xsd:sequence>
            <!-- sifra MM: EIC kod mjernog mjesta-->
            <xsd:element name="Identification" type="mdt:DomainIdentifierType_000122" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="MeteringPointID" minOccurs="1" maxOccurs="1">
                <xsd:simpleType>
            		<xsd:restriction base="xsd:string">
            			<xsd:pattern value="[3][6][Z][A-Z0-9-]{12}[A-Z0-9]{1}"/>
            			<xsd:length value="16"/>
            	    </xsd:restriction>
            	</xsd:simpleType>
            </xsd:element>  
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="MeteringPointUsedDomainLocation" type="mie:DomainLocation"/>   
    <!-- ================================================================================ -->
    <!-- ==== EnergyDocument Type                                                    ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyDocument">
        <xsd:sequence>
            <xsd:element name="Identification" type="mdt:TransactionIdentifierType_000361" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="DocumentType" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:DocumentNameCodeType_000098">
                        <xsd:enumeration value="E21"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
            <xsd:element name="Creation" minOccurs="1" maxOccurs="1">
            	            <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            	        </xsd:element>
            <xsd:element ref="mie:SenderEnergyParty" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:RecipientEnergyParty" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <!-- ================================================================================ -->
    <!-- ==== EnergyContext Type                                                     ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyContext">
        <xsd:sequence>
            <xsd:element name="EnergyBusinessProcess" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:BusinessReasonCodeType_000115">
                        <xsd:enumeration value="E0G"/>                        
                        <xsd:enumeration value="E03"/>
                        <xsd:enumeration value="E34"/>
                        <xsd:enumeration value="E20"/>
                        <xsd:enumeration value="E32"/>
                        <xsd:enumeration value="E56"/>
                        <xsd:enumeration value="E57"/>
                        <xsd:enumeration value="E65"/>
                        <xsd:enumeration value="E66"/>
                        <xsd:enumeration value="E68"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
            <xsd:element name="EnergyBusinessProcessRole" minOccurs="1" maxOccurs="1">
            <xsd:simpleType>
            	<xsd:restriction base="mdt:BusinessRoleCodeType_000113">
            	                <xsd:enumeration value="DDE"/>
            	                <xsd:enumeration value="DDK"/>
            	                <xsd:enumeration value="DDM"/>
            	                <xsd:enumeration value="DDQ"/>
            	                <xsd:enumeration value="DEA"/>
            	                <xsd:enumeration value="MDR"/>
            	                <xsd:enumeration value="RCR"/>
            	                <xsd:enumeration value="TCR"/>
                </xsd:restriction>
            </xsd:simpleType>
            </xsd:element>
            <xsd:element name="EnergyIndustryClassification" minOccurs="0" maxOccurs="1">
            <xsd:simpleType>
            	<xsd:restriction base="mdt:SectorAreaIdentificationCodeType_000116">
            	                <xsd:enumeration value="23"/>
            	                <xsd:enumeration value="27"/>
                </xsd:restriction>
            </xsd:simpleType>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
     <!-- ================================================================================ -->
    <!-- ==== CustomerParty Type                                                     ==== -->
    <!-- ================================================================================ -->
      <xsd:complexType name="CustomerParty">
        <xsd:sequence>
            <!-- Naziv kupca -->
            <xsd:element name="CustomerName" type="mdt:TextType_000114" minOccurs="1" maxOccurs="1"/>
            <!-- Identifikator kupca kod ODS-a-->
            <xsd:element name="SupplierCustomerID" minOccurs="1" maxOccurs="1">
            <xsd:simpleType>
            		<xsd:restriction base="xsd:string">
            			<xsd:maxLength value="16"/>
            	    </xsd:restriction>
            	</xsd:simpleType>
            </xsd:element>   
            <!-- Jedinstveni identifikacioni broj kupca kod snabdjevaca-->
            <xsd:element name="UniqueIDNumber" type="mdt:TextType_000114" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="CustomerIDType" type="mdt:IdentificationTypeCode_000129" minOccurs="1" maxOccurs="1"/>
            <!-- Poreski identifikacioni broj-->
            <xsd:element name="VATNumber" minOccurs="0" maxOccurs="1">         	
     			<xsd:simpleType>
            		<xsd:restriction base="xsd:string">
            			<xsd:maxLength value="13"/>
            	    </xsd:restriction>
            	</xsd:simpleType>
            </xsd:element> 
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="ConsumerInvolvedCustomerParty" type="mie:CustomerParty"/>
    <!-- ================================================================================ -->
    <!-- ==== ContactDetails Type                                                     ==== -->
    <!-- ================================================================================ -->
      <xsd:complexType name="ContactDetails">
        <xsd:sequence>
            <xsd:element name="ContactName" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="ContactType" type="mdt:ContactFunctionTypeCode_000129" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="ContactDetails" type="mie:CustomerParty"/>
<!-- ================================================================================ -->
    <!-- ==== CommunicationDetails Type                                                     ==== -->
    <!-- ================================================================================ -->
      <xsd:complexType name="CommunicationDetails">
        <xsd:sequence>
            <xsd:element name="Sequence" type="mdt:IntegerQuantityType_000119" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="CommunicationChannel" type="mdt:CommunicationChannel_000129" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="CommunicationAddress" type="mdt:TextType_000114" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="PreferredChannel" type="mdt:Boolean_000114" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="CommunicationDetails" type="mie:CommunicationDetails"/>
    <!-- ================================================================================ -->
    <!-- ==== CustomerAddress Type                                              ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="CustomerAddress">
        <xsd:sequence>
            <xsd:element name="CustomerAddressType" type="mdt:AddressTypeCode_000129" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="Postcode" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="BuildingNumber" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="RoomIdentification" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="FloorIdentification" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="StreetName" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="CityName" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="CountryName" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="MunicipalityName" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="CustomerAddress" type="mie:CustomerAddress"/>
    <!-- ================================================================================ -->
    <!-- ==== MasterDataMPEvent Type                                                 ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="MasterDataMPEvent">
        <xsd:sequence>
            <xsd:element name="Identification" type="mdt:TransactionIdentifierType_000361" minOccurs="0" maxOccurs="1"/>
            <!--  ID referentnog zahtjeva na koji se odnosi poruka odbijanja zahtjeva -->            
            <xsd:element name="ReferencetoRequestingTransactionID" type="mdt:TransactionIdentifierType_000361" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="Occurrence" minOccurs="1" maxOccurs="1">
            	            <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            	        </xsd:element>
            <xsd:element name="BusinessProcessReference" type="mdt:TransactionIdentifierType_000361" minOccurs="0" maxOccurs="1"/>
            <xsd:element ref="mie:MeteringPointUsedDomainLocation" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:BalanceSupplier" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:CustomerAddress" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:ConsumerInvolvedCustomerParty" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:CommunicationDetails" minOccurs="1" maxOccurs="unbounded"/>
            <xsd:element ref="mie:EnergySupplyContract" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <!-- ================================================================================ -->
    <!-- ==== BalanceSupplier Type                                                       ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="BalanceSupplier">
        <xsd:sequence>
    		<!-- Identifikator novog snabdjevača -->
     		<xsd:element name="SupplierID" minOccurs="1" maxOccurs="1">
     			<xsd:simpleType>
            		<xsd:restriction base="xsd:string">
            			<xsd:maxLength value="16"/>
            	    </xsd:restriction>
            	</xsd:simpleType>
            </xsd:element> 
            <!-- Naziv novog snabdjevača -->
     		<xsd:element name="SupplierName" minOccurs="1" maxOccurs="1">
     			<xsd:simpleType>
            		<xsd:restriction base="xsd:string">
            			<xsd:maxLength value="200"/>
            	    </xsd:restriction>
            	</xsd:simpleType>
            </xsd:element> 
           <!-- Kontakt telefon novog snabdjevača -->
     		<xsd:element name="SupplierContactPhoneNumber" minOccurs="1" maxOccurs="1">
     			<xsd:simpleType>
            		<xsd:restriction base="xsd:string">
            			<xsd:maxLength value="100"/>
            	    </xsd:restriction>
            	</xsd:simpleType>
            </xsd:element>  
            <!-- Kontakt telefon novog snabdjevača -->
     		<xsd:element name="SupplierContactEmailAddress" minOccurs="1" maxOccurs="1">
     			<xsd:simpleType>
            		<xsd:restriction base="xsd:string">
            			<xsd:maxLength value="100"/>
            	    </xsd:restriction>
            	</xsd:simpleType>
            </xsd:element>      
            </xsd:sequence>
    </xsd:complexType> 
    <xsd:element name="BalanceSupplier" type="mie:BalanceSupplier"/>
    <!-- ================================================================================ -->
    <!-- ==== EnergySupplyContract Type                                      ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergySupplyContract">
        <xsd:sequence>
         <!-- Identifikator ugovora -->
         <xsd:element name="ContractID" type="mdt:TextType_000114" minOccurs="1" maxOccurs="1"/>
         <!-- Datum pocetka vazenja ugovora -->
         <xsd:element name="ContractStartDate" minOccurs="1" maxOccurs="1">
            	 <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            </xsd:element>
         <!-- Datum zavrsetka vazenja ugovora -->
         <xsd:element name="ContractEndDate" minOccurs="1" maxOccurs="1">
            	 <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            </xsd:element>                
         </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="EnergySupplyContract" type="mie:EnergySupplyContract"/>
</xsd:schema>
