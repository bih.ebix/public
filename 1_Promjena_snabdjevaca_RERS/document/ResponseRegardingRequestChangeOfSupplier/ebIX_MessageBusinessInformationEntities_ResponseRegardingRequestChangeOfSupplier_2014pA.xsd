<?xml version="1.0" encoding="UTF-8"?>
<!-- ================================================================================ -->
<!-- ==== Message Business Information Entities XML Schema Module                ==== -->
<!-- ================================================================================ -->
<!--
Schema agency:     ebIX
Schema version:    2014.A
Schema date:       January 25, 2017

Copyright (C) ebIX (2010). All Rights Reserved.

This document is created by using the ebIX Transformation Tool version of November 2016.

This document and translations of it may be copied and furnished to others,
and derivative works that comment on or otherwise explain it or assist in
its implementation may be prepared, copied, published and distributed, in
whole or in part, without restriction of any kind, provided that the above
copyright notice and this paragraph are included on all such copies and
derivative works. However, this document itself may not be modified in any
way, such as by removing the copyright notice or references to ebIX, except
as needed for the purpose of developing ebIX specifications, in which case
the procedures for copyrights defined in the UN/CEFACT Intellectual Property
Rights document must be followed, or as required to translate it into
languages other than English.
The limited permissions granted above are perpetual and will not be revoked
by ebIX or its successors or assigns.
This document and the information contained herein is provided on an "AS IS"
basis and ebIX DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT
INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS
FOR A PARTICULAR PURPOSE.
-->
<xsd:schema
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:mie="un:unece:260:data:EEM-NegativeResponseRequestValidatedDataForBilling"
    xmlns:ccts="urn:un:unece:uncefact:documentation:common:3:standard:CoreComponentsTechnicalSpecification:3"
    xmlns:xbt="urn:un:unece:uncefact:data:common:1:draft"
    xmlns:bie="un:unece:260:data:EEM"
    xmlns:mdt="un:unece:260:data:EEM-NegativeResponseRequestValidatedDataForBilling"
    targetNamespace="un:unece:260:data:EEM-NegativeResponseRequestValidatedDataForBilling"
    elementFormDefault="qualified"
    attributeFormDefault="unqualified"
    version="2014.A">
    <!-- ================================================================================ -->
    <!-- ==== Imports                                                                ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== Inclusions                                                             ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== Inclusion of Message Data Types                                        ==== -->
    <!-- ================================================================================ -->
    <xsd:include schemaLocation="ebIX_MessageDataType_ResponseRegardingRequestChangeOfSupplier_2014pA.xsd"/>
    
    <!-- ================================================================================ -->
    <!-- ==== Message Business Information Entities Definitions                      ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== EnergyParty Type                                                       ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyParty">
        <xsd:sequence>
            <xsd:element name="Identification" type="mdt:PartyIdentifierType_000112" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="SenderEnergyParty" type="mie:EnergyParty"/>
    <xsd:element name="RecipientEnergyParty" type="mie:EnergyParty"/>
    <!-- ================================================================================ -->
    <!-- ==== DomainLocation Type                                                    ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="DomainLocation">
        <xsd:sequence>
     		<!-- Identifikator mjernog mjesta -->
     		<xsd:element name="MeteringPointID" minOccurs="1" maxOccurs="1">
     			<xsd:simpleType>
            		<xsd:restriction base="xsd:string">
            			<xsd:pattern value="[3][6][Z][A-Z0-9-]{12}[A-Z0-9]{1}"/>
            			<xsd:maxLength value="16"/>
            	    </xsd:restriction>
            	</xsd:simpleType>
            </xsd:element>  
            <!-- Naziv mjernog mjesta-->
            <xsd:element name="MeteringPointName" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>     	            	                 
            <!-- Angažovana snaga -->
            <xsd:element name="ContractedConnectionCapacity" type="mdt:MeasureType_000358" minOccurs="0" maxOccurs="1"/>
            <!-- Mjerna jedinica za angažovanu snagu -->
            <xsd:element name="ContractedConnectionCapacityMeasureUnit" type="mdt:MeasurementUnitCommonCodeType_000121" minOccurs="0" maxOccurs="1"/>
            <!-- Naponski nivo mjernog mjesta -->
            <xsd:element name="VoltageLevel" type="mdt:VoltageLevelCodeType_000150" minOccurs="0" maxOccurs="1"/>
            <!-- Kategorija MM -->
            <xsd:element name="AccountingPointCategory" type="mdt:APCategoryType_000123" minOccurs="1" maxOccurs="1"/>
        	<!-- Tarifna grupa MM -->
            <xsd:element name="TariffGroup" type="mdt:TariffGroupType_000123" minOccurs="1" maxOccurs="1"/>            
            <!-- Postanski kod u adresi MM-->
          	<xsd:element name="APPostcode" type="mdt:IntegerQuantityType_000119" minOccurs="0" maxOccurs="1"/>    
            <!-- Ulicni broj a u adresi MM-->
            <xsd:element name="APBuildingNumber" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <!-- Broj stana u adresi MM-->
            <xsd:element name="APRoomIdentification" type="mdt:IntegerQuantityType_000119" minOccurs="0" maxOccurs="1"/>
            <!-- Broj sprata u adresi MM-->
            <xsd:element name="APFloorIdentification" type="mdt:IntegerQuantityType_000119" minOccurs="0" maxOccurs="1"/>
            <!-- Naziv ulice u adresi MM-->
            <xsd:element name="APStreetName" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <!-- Naziv grada u adresi MM-->
            <xsd:element name="APCityName" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <!-- Naziv drzave u adresi MM-->
            <xsd:element name="APCountryName" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>
            <!-- Naziv opstine u adresi MM-->
            <xsd:element name="APMunicipalityName" type="mdt:TextType_000114" minOccurs="0" maxOccurs="1"/>   
            </xsd:sequence>
    </xsd:complexType> 
    <xsd:element name="MeteringPointUsedDomainLocation" type="mie:DomainLocation"/>
     <!-- ================================================================================ -->
    <!-- ==== CustomerParty Type                                                     ==== -->
    <!-- ================================================================================ -->
      <xsd:complexType name="CustomerParty">
        <xsd:sequence>
            <!-- Naziv kupca -->
            <xsd:element name="CustomerName" type="mdt:TextType_000114" minOccurs="1" maxOccurs="1"/>
            <!-- Identifikator kupca kod ODS-a-->
            <xsd:element name="SupplierCustomerID" minOccurs="1" maxOccurs="1">
            <xsd:simpleType>
            		<xsd:restriction base="xsd:string">
            			<xsd:maxLength value="16"/>
            	    </xsd:restriction>
            	</xsd:simpleType>
            </xsd:element>   
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="ConsumerInvolvedCustomerParty" type="mie:CustomerParty"/>
    <!-- ================================================================================ -->
    <!-- ==== EnergyDocument Type                                                    ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyDocument">
        <xsd:sequence>
            <xsd:element name="Identification" type="mdt:TransactionIdentifierType_000361" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="DocumentType" minOccurs="1" maxOccurs="1">
            <xsd:simpleType>
            	<xsd:restriction base="mdt:DocumentNameCodeType_000098">
            	                <xsd:enumeration value="E68"/>
                </xsd:restriction>
            </xsd:simpleType>
            </xsd:element>
            <xsd:element name="Creation" minOccurs="1" maxOccurs="1">
            	            <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            	        </xsd:element>
            <xsd:element ref="mie:SenderEnergyParty" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:RecipientEnergyParty" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <!-- ================================================================================ -->
    <!-- ==== EnergyContext Type                                                     ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyContext">
        <xsd:sequence>
            <xsd:element name="EnergyBusinessProcess" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:BusinessReasonCodeType_000115">
                        <xsd:enumeration value="E03"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
            <xsd:element name="EnergyBusinessProcessRole" minOccurs="1" maxOccurs="1">
            <xsd:simpleType>
                    <xsd:restriction base="mdt:BusinessRoleCodeType_000113">
                        <xsd:enumeration value="MDR"/>
                    </xsd:restriction>
            </xsd:simpleType>
            </xsd:element>
            <xsd:element name="EnergyIndustryClassification" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:SectorAreaIdentificationCodeType_000116">
                        <xsd:enumeration value="23"/>
                        <xsd:enumeration value="27"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
    <!-- ================================================================================ -->
    <!-- ==== ResponseEvent Type                                                     ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="ResponseEvent">
        <xsd:sequence>
            <!-- Identifikator naloga za odbijanje mogućnosti zaključenja UOS -->
            <xsd:element name="Identification" type="mdt:TransactionIdentifierType_000361" minOccurs="1" maxOccurs="1"/>
           <!-- Identifikator zahtjeva za promjenu snabdjevača na koji se odnosi zahtjev za dopunu podataka, npr: REoS_12345678  -->
            <xsd:element name="ReferenceToRequestingTransactionID" type="mdt:TransactionIdentifierType_000361" minOccurs="1" maxOccurs="1"/>
            <!-- Datum podnošenja zahtjeva za promjenu (kupac) -->
            <xsd:element name="StartOfOccurrence" minOccurs="1" maxOccurs="1">
            	            <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            </xsd:element>        
          <!-- Reagovanje na obavještenje o promjeni snabdjevača -->
          <xsd:element name="Response" minOccurs="1"
				maxOccurs="1">
				<xsd:simpleType>
					<xsd:restriction base="mdt:TextType_000114">
						<xsd:enumeration value="Confirm" />
						<xsd:enumeration value="Reject" />
					</xsd:restriction>
				</xsd:simpleType>
			</xsd:element>
		 <!-- Kratak opis razloga za odbijanje promjenu snabdjevača -->
             <xsd:element name="Information" minOccurs="1" maxOccurs="1">
            	            <xsd:simpleType>
            					<xsd:restriction base="xsd:string">
            						<xsd:maxLength value="256"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            </xsd:element>
            <xsd:element ref="mie:MeteringPointUsedDomainLocation" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:ConsumerInvolvedCustomerParty" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
</xsd:schema>
