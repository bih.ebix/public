<?xml version="1.0" encoding="UTF-8"?>
<!-- ================================================================================ -->
<!-- ==== Message Business Information Entities XML Schema Module                ==== -->
<!-- ================================================================================ -->
<!--
Schema agency:     ebIX
Schema version:    2014.A
Schema date:       January 25, 2017

Copyright (C) ebIX (2010). All Rights Reserved.

This document is created by using the ebIX Transformation Tool version of November 2016.

This document and translations of it may be copied and furnished to others,
and derivative works that comment on or otherwise explain it or assist in
its implementation may be prepared, copied, published and distributed, in
whole or in part, without restriction of any kind, provided that the above
copyright notice and this paragraph are included on all such copies and
derivative works. However, this document itself may not be modified in any
way, such as by removing the copyright notice or references to ebIX, except
as needed for the purpose of developing ebIX specifications, in which case
the procedures for copyrights defined in the UN/CEFACT Intellectual Property
Rights document must be followed, or as required to translate it into
languages other than English.
The limited permissions granted above are perpetual and will not be revoked
by ebIX or its successors or assigns.
This document and the information contained herein is provided on an "AS IS"
basis and ebIX DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT
INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS
FOR A PARTICULAR PURPOSE.
-->
<xsd:schema
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:mie="un:unece:260:data:EEM-ValidatedDataForBillingEnergy"
    xmlns:ccts="urn:un:unece:uncefact:documentation:common:3:standard:CoreComponentsTechnicalSpecification:3"
    xmlns:xbt="urn:un:unece:uncefact:data:common:1:draft"
    xmlns:bie="un:unece:260:data:EEM"
    xmlns:mdt="un:unece:260:data:EEM-ValidatedDataForBillingEnergy"
    targetNamespace="un:unece:260:data:EEM-ValidatedDataForBillingEnergy"
    elementFormDefault="qualified"
    attributeFormDefault="unqualified"
    version="2014.A">
    <!-- ================================================================================ -->
    <!-- ==== Imports                                                                ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== Inclusions                                                             ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== Inclusion of Message Data Types                                        ==== -->
    <!-- ================================================================================ -->
    <xsd:include schemaLocation="ebIX_MessageDataType_ValidatedDataForBillingEnergy_2014pA.xsd"/>
    
    <!-- ================================================================================ -->
    <!-- ==== Message Business Information Entities Definitions                      ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== EnergyParty Type                                                       ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyParty">
        <xsd:sequence>
            <xsd:element name="Identification" type="mdt:PartyIdentifierType_000112" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="SenderEnergyParty" type="mie:EnergyParty"/>
    <xsd:element name="RecipientEnergyParty" type="mie:EnergyParty"/>
    <!-- ================================================================================ -->
    <!-- ==== DomainLocation Type                                                    ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="DomainLocation">
        <xsd:sequence>
            <xsd:element name="Identification" type="mdt:DomainIdentifierType_000122" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="MeteringPointUsedDomainLocation" type="mie:DomainLocation"/>
    <!-- ================================================================================ -->
    <!-- ==== EnergyTimeSeries Type                                                  ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyTimeSeries">
        <xsd:sequence>
            <xsd:element name="Identification" type="mdt:TransactionIdentifierType_000361" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="RegistrationDateTime" minOccurs="0" maxOccurs="1">
            	            <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            	        </xsd:element>
            <xsd:element name="RequestReference" type="mdt:TransactionIdentifierType_000361" minOccurs="0" maxOccurs="1"/>
            <xsd:element ref="mie:ObservationPeriodTimeSeriesPeriod" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:ProductIncludedProductCharacteristic" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:MPDetailMeasurementMeteringPointCharacteristic" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:MeteringPointUsedDomainLocation" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:MeteringInstallationMeterFacility" minOccurs="0" maxOccurs="1"/>
            <xsd:element ref="mie:ObservationIntervalObservationPeriod" minOccurs="1" maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>
    <!-- ================================================================================ -->
    <!-- ==== TimeSeriesPeriod Type                                                  ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="TimeSeriesPeriod">
        <xsd:sequence>
            <xsd:element name="ResolutionDuration" minOccurs="0" maxOccurs="1">
            <xsd:simpleType>
            	<xsd:restriction base="mdt:DurationType_000151">
            	                <xsd:enumeration value="PT15M"/>
                </xsd:restriction>
            </xsd:simpleType>
            </xsd:element>
            <xsd:element name="Start" minOccurs="1" maxOccurs="1">
            	            <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            	        </xsd:element>
            <xsd:element name="End" minOccurs="1" maxOccurs="1">
            	            <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            	        </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="ObservationPeriodTimeSeriesPeriod" type="mie:TimeSeriesPeriod"/>
    <!-- ================================================================================ -->
    <!-- ==== ProductCharacteristic Type                                             ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="ProductCharacteristic">
        <xsd:sequence>
            <xsd:element name="Identification" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:EnergyProductIdentifierType_000120">
                        <xsd:enumeration value="8716867000016"/>
                        <xsd:enumeration value="8716867000023"/>
                        <xsd:enumeration value="8716867000030"/>
                        <xsd:enumeration value="8716867000047"/>
                        <xsd:enumeration value="8716867000031"/>
                        <xsd:enumeration value="8716867000032"/>
                        <xsd:enumeration value="8716867000033"/>
                        <xsd:enumeration value="8716867000034"/>
                        <xsd:enumeration value="8716867000035"/>
                        <xsd:enumeration value="8716867000036"/>
                        <xsd:enumeration value="8716867000037"/>
                        <xsd:enumeration value="8716867000038"/>
                        <xsd:enumeration value="8716867000039"/>
                        <xsd:enumeration value="8716867000040"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
            <xsd:element name="UnitType" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:MeasurementUnitCommonCodeType_000121">
                        <xsd:enumeration value="KWH"/>
                        <xsd:enumeration value="NM3"/>
                        <xsd:enumeration value="SM3"/>
                        <xsd:enumeration value="KWT"/>
                        <xsd:enumeration value="K3"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="ProductIncludedProductCharacteristic" type="mie:ProductCharacteristic"/>
    <!-- ================================================================================ -->
    <!-- ==== EnergyDocument Type                                                    ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyDocument">
        <xsd:sequence>
            <xsd:element name="Identification" type="mdt:TransactionIdentifierType_000361" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="DocumentType" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:DocumentNameCodeType_000098">
                        <xsd:enumeration value="E66"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
            <xsd:element name="Creation" minOccurs="1" maxOccurs="1">
            	            <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            	        </xsd:element>
            <xsd:element ref="mie:SenderEnergyParty" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:RecipientEnergyParty" minOccurs="1" maxOccurs="1"/>
        </xsd:sequence>
    </xsd:complexType>
    <!-- ================================================================================ -->
    <!-- ==== RegisterFacility Type                                                  ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="RegisterFacility">
        <xsd:sequence>
            <xsd:element name="RegisterIdentification" type="mdt:EEMIdentifierType_000110" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:MeterReadEnergyMeterRead" minOccurs="1" maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="RegisterInstallationRegisterFacility" type="mie:RegisterFacility"/>
    <!-- ================================================================================ -->
    <!-- ==== EnergyContext Type                                                     ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyContext">
        <xsd:sequence>
            <xsd:element name="EnergyBusinessProcess" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:BusinessReasonCodeType_000115">
                        <xsd:enumeration value="E88"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
            <xsd:element name="EnergyBusinessProcessRole" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:BusinessRoleCodeType_000113">
                        <xsd:enumeration value="DDQ"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
            <xsd:element name="EnergyIndustryClassification" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:SectorAreaIdentificationCodeType_000116">
                        <xsd:enumeration value="23"/>
                        <xsd:enumeration value="27"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
    <!-- ================================================================================ -->
    <!-- ==== MeasurementMeteringPointCharacteristic Type                            ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="MeasurementMeteringPointCharacteristic">
        <xsd:sequence>
            <xsd:element name="MeteringPointType" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:MeteringPointTypeCodeType_000138">
                        <xsd:enumeration value="E17"/>
                        <xsd:enumeration value="E18"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="MPDetailMeasurementMeteringPointCharacteristic" type="mie:MeasurementMeteringPointCharacteristic"/>
    <!-- ================================================================================ -->
    <!-- ==== ObservationPeriod Type                                                 ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="ObservationPeriod">
        <xsd:sequence>
            <xsd:element name="Sequence" type="mdt:OrdinalType_000123" minOccurs="1" maxOccurs="1"/>
            <xsd:element ref="mie:ObservationDetailEnergyObservation" minOccurs="1" maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="ObservationIntervalObservationPeriod" type="mie:ObservationPeriod"/>
    <!-- ================================================================================ -->
    <!-- ==== EnergyObservation Type                                                 ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyObservation">
        <xsd:sequence>
            <xsd:element name="EnergyQuantity" type="mdt:IntegerQuantityType_000119" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="MeterTimeFrameType" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:MeterTimeFrameCodeType_000126">
                        <xsd:enumeration value="E13"/>
                        <xsd:enumeration value="E14"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="ObservationDetailEnergyObservation" type="mie:EnergyObservation"/>
    <!-- ================================================================================ -->
    <!-- ==== EnergyMeterRead Type                                                   ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="EnergyMeterRead">
        <xsd:sequence>
            <xsd:element name="ReadDateTime" minOccurs="0" maxOccurs="1">
            	            <xsd:simpleType>
            	            	<xsd:restriction base="xsd:dateTime">
            		                    <xsd:pattern value="[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"/>
            	                </xsd:restriction>
            	            </xsd:simpleType>
            	        </xsd:element>
            <xsd:element name="ReadMeasure" type="mie:ReadQuantityType_000...tbd" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="MeterReadingOriginType" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
                <xsd:simpleContent>
                    <xsd:restriction base="mdt:MeterReadingOriginCodeType_000139">
                        <xsd:enumeration value="E26"/>
                        <xsd:enumeration value="E27"/>
                        <xsd:enumeration value="E28"/>
                    </xsd:restriction>
                </xsd:simpleContent>
            </xsd:complexType>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="MeterReadEnergyMeterRead" type="mie:EnergyMeterRead"/>
    <!-- ================================================================================ -->
    <!-- ==== MeterFacility Type Promjenjen tip MeterIdentification u integer  i dodan BillingMultiplier      ==== -->
    <!-- ================================================================================ -->
    <xsd:complexType name="MeterFacility">
        <xsd:sequence>
            <xsd:element name="MeterIdentification" type="xsd:integer" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="BillingMultiplier" minOccurs="0" maxOccurs="1">
            	<xsd:simpleType>
	    				<xsd:restriction base="xsd:integer">
	     				</xsd:restriction>
 				</xsd:simpleType>
 			</xsd:element> 
            <xsd:element ref="mie:RegisterInstallationRegisterFacility" minOccurs="1" maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="MeteringInstallationMeterFacility" type="mie:MeterFacility"/>
</xsd:schema>
