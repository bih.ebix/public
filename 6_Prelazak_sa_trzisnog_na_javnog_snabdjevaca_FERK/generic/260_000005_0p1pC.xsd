<?xml version="1.0" encoding="UTF-8"?>
<!-- ================================================================================ -->
<!-- ==== SubsetResponseReasonDescriptionCode - Code List Schema Module          ==== -->
<!-- ================================================================================ -->
<!--
Schema agency:     ebIX
Schema version:    0.1.C
Schema date:       January 25, 2017

Code list name:    ResponseReasonDescriptionCode
Code list agency:  ebix 
Code list version: 0.1.C

Copyright (C) ebIX (2010). All Rights Reserved.

This document is created by using the ebIX Transformation Tool version of November 2016.

This document and translations of it may be copied and furnished to others,
and derivative works that comment on or otherwise explain it or assist in
its implementation may be prepared, copied, published and distributed, in
whole or in part, without restriction of any kind, provided that the above
copyright notice and this paragraph are included on all such copies and
derivative works. However, this document itself may not be modified in any
way, such as by removing the copyright notice or references to ebIX, except
as needed for the purpose of developing ebIX specifications, in which case
the procedures for copyrights defined in the UN/CEFACT Intellectual Property
Rights document must be followed, or as required to translate it into
languages other than English.
The limited permissions granted above are perpetual and will not be revoked
by ebIX or its successors or assigns.
This document and the information contained herein is provided on an "AS IS"
basis and ebIX DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT
INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS
FOR A PARTICULAR PURPOSE.
-->
<xsd:schema
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:bcl260_000005_01C="un:unece:260:data:EEM"
    xmlns:ccts="urn:un:unece:uncefact:documentation:common:3:standard:CoreComponentsTechnicalSpecification:3"
    xmlns:xbt="urn:un:unece:uncefact:data:common:1:draft"
    targetNamespace="un:unece:260:data:EEM"
    elementFormDefault="qualified"
    attributeFormDefault="unqualified"
    version="0.1.C">
    <!-- ================================================================================ -->
    <!-- ==== Imports                                                                ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== Root element for this code list                                        ==== -->
    <!-- ================================================================================ -->
    <xsd:element name="SubsetResponseReasonDescriptionCode" 
        type="bcl260_000005_01C:SubsetResponseReasonDescriptionCodeContentType"/>

    <!-- ================================================================================ -->
    <!-- ==== Content type for this code list                                        ==== -->
    <!-- ================================================================================ -->
    <xsd:simpleType name="SubsetResponseReasonDescriptionCodeContentType">
        <xsd:restriction base="xsd:token">
        <xsd:enumeration value="E09">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E09</ccts:Name>
                 <ccts:Description>Installation not identifiable</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E10">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E10</ccts:Name>
                 <ccts:Description>Metering point not identifiable</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E11">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E11</ccts:Name>
                 <ccts:Description>Measuring problem</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E12">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E12</ccts:Name>
                 <ccts:Description>Unclear delivery relation</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E13">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E13</ccts:Name>
                 <ccts:Description>Balancing problem</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E14">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E14</ccts:Name>
                 <ccts:Description>Other reason</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E15">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E15</ccts:Name>
                 <ccts:Description>No corrections</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E16">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E16</ccts:Name>
                 <ccts:Description>Unauthorised Balance supplier</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E17">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E17</ccts:Name>
                 <ccts:Description>Requested switch date not within time limits</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E18">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E18</ccts:Name>
                 <ccts:Description>Unauthorised balance responsible</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E19">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E19</ccts:Name>
                 <ccts:Description>Meter stand not within limits</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E22">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E22</ccts:Name>
                 <ccts:Description>Metering point blocked for switching</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E29">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E29</ccts:Name>
                 <ccts:Description>Product code unknown or not related to MP</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E36">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E36</ccts:Name>
                 <ccts:Description>No valid collaboration</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E37">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E37</ccts:Name>
                 <ccts:Description>No valid grid access contract</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E47">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E47</ccts:Name>
                 <ccts:Description>No ongoing switch for MP</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E49">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E49</ccts:Name>
                 <ccts:Description>Metering grid area not identifiable</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E50">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E50</ccts:Name>
                 <ccts:Description>Invalid period</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E51">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E51</ccts:Name>
                 <ccts:Description>Invalid number of decimals</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E52">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E52</ccts:Name>
                 <ccts:Description>Invalid load profile</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E54">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E54</ccts:Name>
                 <ccts:Description>Unauthorised transport capacity responsible</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E55">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E55</ccts:Name>
                 <ccts:Description>Unauthorized Metered Data Responsible</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E59">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E59</ccts:Name>
                 <ccts:Description>Already existing relation</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E61">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E61</ccts:Name>
                 <ccts:Description>Meter not identifiable</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E62">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E62</ccts:Name>
                 <ccts:Description>Register not identifiable</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E72">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E72</ccts:Name>
                 <ccts:Description>Asset not identifiable</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E73">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E73</ccts:Name>
                 <ccts:Description>Incorrect measure unit</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E86">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E86</ccts:Name>
                 <ccts:Description>Incorrect value</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E87">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E87</ccts:Name>
                 <ccts:Description>Number of observations doesn&apos;t fit observation period/resolution</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E97">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E97</ccts:Name>
                 <ccts:Description>Measurement should not be zero</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E98">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E98</ccts:Name>
                 <ccts:Description>Measurement has a wrong sign</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E81">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E81</ccts:Name>
                 <ccts:Description>MeteringPoint is not connected</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E90">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E90</ccts:Name>
                 <ccts:Description>Measurement beyond plausibility limits</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E91">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E91</ccts:Name>
                 <ccts:Description>Estimate is not acceptable</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0H">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0H</ccts:Name>
                 <ccts:Description>Data not available</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0I">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0I</ccts:Name>
                 <ccts:Description>Unauthorised Grid Access Provider</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0J">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0J</ccts:Name>
                 <ccts:Description>Unauthorised Metered Data Aggregator</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0K">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0K</ccts:Name>
                 <ccts:Description>Unauthorised Metered Data Collector</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0L">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0L</ccts:Name>
                 <ccts:Description>Unauthorised Party Connected to the Grid</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0M">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0M</ccts:Name>
                 <ccts:Description>Unauthorised Reconciliation Responsible</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0Q">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0Q</ccts:Name>
                 <ccts:Description>Unauthorised Meter Operator</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0R">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0R</ccts:Name>
                 <ccts:Description>Unauthorised Calorific Value Responsible</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="CMP">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>CMP</ccts:Name>
                 <ccts:Description>Old Balance Supplier submitted complaint for CoS request</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        </xsd:restriction>
    </xsd:simpleType>
</xsd:schema>  
