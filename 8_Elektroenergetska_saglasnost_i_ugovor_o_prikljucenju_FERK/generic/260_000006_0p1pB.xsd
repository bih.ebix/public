<?xml version="1.0" encoding="UTF-8"?>
<!-- ================================================================================ -->
<!-- ==== SubsetBusinessReasonCode - Code List Schema Module                     ==== -->
<!-- ================================================================================ -->
<!--
Schema agency:     ebIX
Schema version:    0.1.B
Schema date:       January 25, 2017

Code list name:    BusinessReasonCode
Code list agency:  ebix 
Code list version: 0.1.B

Copyright (C) ebIX (2010). All Rights Reserved.

This document is created by using the ebIX Transformation Tool version of November 2016.

This document and translations of it may be copied and furnished to others,
and derivative works that comment on or otherwise explain it or assist in
its implementation may be prepared, copied, published and distributed, in
whole or in part, without restriction of any kind, provided that the above
copyright notice and this paragraph are included on all such copies and
derivative works. However, this document itself may not be modified in any
way, such as by removing the copyright notice or references to ebIX, except
as needed for the purpose of developing ebIX specifications, in which case
the procedures for copyrights defined in the UN/CEFACT Intellectual Property
Rights document must be followed, or as required to translate it into
languages other than English.
The limited permissions granted above are perpetual and will not be revoked
by ebIX or its successors or assigns.
This document and the information contained herein is provided on an "AS IS"
basis and ebIX DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT
INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS
FOR A PARTICULAR PURPOSE.
-->
<xsd:schema
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:bcl260_000006_01B="un:unece:260:data:EEM"
    xmlns:ccts="urn:un:unece:uncefact:documentation:common:3:standard:CoreComponentsTechnicalSpecification:3"
    xmlns:xbt="urn:un:unece:uncefact:data:common:1:draft"
    targetNamespace="un:unece:260:data:EEM"
    elementFormDefault="qualified"
    attributeFormDefault="unqualified"
    version="0.1.B">
    <!-- ================================================================================ -->
    <!-- ==== Imports                                                                ==== -->
    <!-- ================================================================================ -->
    <!-- ================================================================================ -->
    <!-- ==== Root element for this code list                                        ==== -->
    <!-- ================================================================================ -->
    <xsd:element name="SubsetBusinessReasonCode" 
        type="bcl260_000006_01B:SubsetBusinessReasonCodeContentType"/>

    <!-- ================================================================================ -->
    <!-- ==== Content type for this code list                                        ==== -->
    <!-- ================================================================================ -->
    <xsd:simpleType name="SubsetBusinessReasonCodeContentType">
        <xsd:restriction base="xsd:token">
        <xsd:enumeration value="E01">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E01</ccts:Name>
                 <ccts:Description>Move; change of party connected to the grid</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E02">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E02</ccts:Name>
                 <ccts:Description>New metering point</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E03">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E03</ccts:Name>
                 <ccts:Description>Change of balance supplier</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E04">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E04</ccts:Name>
                 <ccts:Description>Temporary supply</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E05">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E05</ccts:Name>
                 <ccts:Description>Cancellation</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E06">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E06</ccts:Name>
                 <ccts:Description>Unrequested change of balance supplier</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E20">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E20</ccts:Name>
                 <ccts:Description>End of supply</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E21">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E21</ccts:Name>
                 <ccts:Description>Change of data from party connected to the grid</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E23">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E23</ccts:Name>
                 <ccts:Description>Periodic metering</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E28">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E28</ccts:Name>
                 <ccts:Description>Profile maintenance</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E30">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E30</ccts:Name>
                 <ccts:Description>Historical data</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E32">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E32</ccts:Name>
                 <ccts:Description>Update master data metering point</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E34">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E34</ccts:Name>
                 <ccts:Description>Update master data consumer</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E35">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E35</ccts:Name>
                 <ccts:Description>Combined customer and supplier switch</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E43">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E43</ccts:Name>
                 <ccts:Description>Reconciliation</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E44">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E44</ccts:Name>
                 <ccts:Description>Imbalance settlement</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E48">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E48</ccts:Name>
                 <ccts:Description>Master data for prospects</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E53">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E53</ccts:Name>
                 <ccts:Description>Meter reading on demand</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E56">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E56</ccts:Name>
                 <ccts:Description>Change of Balance Responsible Party</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E57">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E57</ccts:Name>
                 <ccts:Description>Change of metered data responsible</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E60">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E60</ccts:Name>
                 <ccts:Description>Removal of meter</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E64">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E64</ccts:Name>
                 <ccts:Description>Update master data MP requiring meter reading</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E65">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E65</ccts:Name>
                 <ccts:Description>Consumer move-in</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E66">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E66</ccts:Name>
                 <ccts:Description>Consumer move-out</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E67">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E67</ccts:Name>
                 <ccts:Description>Placement of meter</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E68">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E68</ccts:Name>
                 <ccts:Description>Change of transport capacity responsible</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E74">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E74</ccts:Name>
                 <ccts:Description>Change of meter administrator</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E75">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E75</ccts:Name>
                 <ccts:Description>Change of metering method</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E77">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E77</ccts:Name>
                 <ccts:Description>End of metering</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E78">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E78</ccts:Name>
                 <ccts:Description>Change of grid billing model</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E84">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E84</ccts:Name>
                 <ccts:Description>Update master data meter</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E88">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E88</ccts:Name>
                 <ccts:Description>Billing energy</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E89">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E89</ccts:Name>
                 <ccts:Description>Billing Grid Cost</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E93">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E93</ccts:Name>
                 <ccts:Description>End of Party connected to Grid</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E94">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E94</ccts:Name>
                 <ccts:Description>End of Metered Data Responsible</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E95">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E95</ccts:Name>
                 <ccts:Description>End of Meter Administrator</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E96">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E96</ccts:Name>
                 <ccts:Description>End of Balance Responsible</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E76">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E76</ccts:Name>
                 <ccts:Description>MeteringPoint Id and additional information do not correspond</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E79">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E79</ccts:Name>
                 <ccts:Description>Change of connection status</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E80">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E80</ccts:Name>
                 <ccts:Description>Change of estimated annual volume</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E99">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E99</ccts:Name>
                 <ccts:Description></ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0A">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0A</ccts:Name>
                 <ccts:Description></ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0B">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0B</ccts:Name>
                 <ccts:Description></ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0C">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0C</ccts:Name>
                 <ccts:Description></ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0D">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0D</ccts:Name>
                 <ccts:Description></ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0E">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0E</ccts:Name>
                 <ccts:Description></ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0F">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0F</ccts:Name>
                 <ccts:Description>Labeling</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0G">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0G</ccts:Name>
                 <ccts:Description>Data alignment master data MP</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0N">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0N</ccts:Name>
                 <ccts:Description>Preparation for Reconciliation</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        <xsd:enumeration value="E0P">
           <xsd:annotation>
              <xsd:documentation xml:lang="en">
                 <ccts:Name>E0P</ccts:Name>
                 <ccts:Description>Preparation for Imbalance Settlement</ccts:Description>
              </xsd:documentation>
           </xsd:annotation>
        </xsd:enumeration>
        </xsd:restriction>
    </xsd:simpleType>
</xsd:schema>  
